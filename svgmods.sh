#!/bin/bash

## add reference to external stylesheet to each svg file

path="www/8/svg"
header='<?xml-stylesheet type="text/css" href="svg-stylesheet.css" ?> '



for i in `ls $path/*.svg`;
do
    # change size - commented out - size can be defined in html
    sed -i 's/  width="100.00mm" height="45.00mm" viewBox="0 0 63.2283 28.4528"/ width="1000px" height="450px" viewBox="0 0 1000 450"/'g $i
    #sed -i 's/ width="105.00mm" height="60.00mm" / width="1280px" height="800px" /'g $i
    echo $header > tmp.svg; 
    cat $i >> tmp.svg ; 
    mv tmp.svg $i;
done;
