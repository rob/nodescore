/////////////////////////////////////////////////
// connect to websocket

var socket = io.connect();

//socket.on("metroPulse", metronomeTick);
/////////////////////////////////////////////////
socket.on("metroPulse", pulseInClient);
function pulseInClient(pulse,groupID,metrobeat){
    var groupPage=document.getElementById('group').value
    if (groupID == groupPage) {
	metronomeTick(1000, groupID, metrobeat);
    }
}
/////////////////////////////////////////////////
function  metroCss(beat, beatcolor,text){
    var color = beatcolor;
    
    $(".metrocase > div").each(function(){$(this).stop()});
    $(".metrocase > div").each(function(){$(this).css('background-color', beatcolor)});
    $(".metrocase > div").each(function(){$(this).text(" ")});
    $(".metrocase > div").each(function(){$(this).animate({opacity: 1},
							  50,
							  function() { $(this).animate({opacity:0.0}); }
							 );
					 })  
	}

function metronomeTick(pulse, voice,metrobeat) {
    var color = "gray"
    metroCss(0, "red", "4")    
};

/////////////////////////////////////////////////

// update the stopwatch value on the client page in line with server
socket.on("chronFromServer", function(chron) {
    $("div#client_chronometer").text(chron);
});


// server time

//var groupPage=document.getElementById('group').value
//if (groupID == groupPage) {
    socket.on("dateTime", function(datetime) {
//	console.log(datetime)
	$("div#datetime").text("Server Time: " +datetime);
	
    });


/////////////////////////////////////////////////
// countdown to change

socket.on("countinFromServer", countinClient);
function countinClient(groupID, currentseconds,mm,text,colour,background){
    var groupPage=document.getElementById('group').value
    if (groupID == groupPage) {
	console.log(groupID, currentseconds,mm,text,colour,background)
	document.getElementById("count").style.visibility="visible";
	//document.getElementById("count").style.visibility="visible";
	$("#counttitle").text(text);
	$("#count").text(currentseconds);
	//$("#cnt").text(currentseconds);
	$("#count").css('background-color', background);	
	document.getElementById("count").style.color=colour;
//	if ( currentseconds == 0) {
	    //document.getElementById("midcomms").style.visibility='hidden';
//	}
    }}
/////////////////////////////////////////////////

/////////////////////////////////////////////////
// countdown to change

socket.on("counterText", cText);
function cText(groupID, currentseconds,text){

    var groupPage=document.getElementById('group').value
    if (groupID == groupPage) {
//	console.log(text)
	$("#totalcountdown").text(text);	
    }}

///////////////////////////////////////

function pageFlip(unit) {
    console.log("flipping page:"+ unit);
    $('#sections').trigger('goto', [parseFloat(unit)]); 
}


/////////////////////////////////////////////////

// call the fancy jquery functions

function slideTo (target) { $('#sections').trigger('goto', [target]); }
function pad2(number) { return (number < 10 ? '0' : '') + number }       

/////////////////////////////////////////////////

socket.on("pageFlipfromserver", pageTurn);
function pageTurn (group,unit,time,mm) {
    var groupPage=document.getElementById('group').value;
    if (group == groupPage) {
	var g= pad2(group); 
	var p= pad2(unit);
//	pageFlip(unit); 
	pageTurnB(unit)
	testSound.play();
    console.log("flipping page:"+ unit);  
 }
    else { 
	console.log("not for this group... ignoring... for group:" + group );
    }
}

function pageTurnB(unit) {
    var units=6;
    var next=(((unit+1)%units)+units)%units
    console.log("HOP TURN" + unit+ "next:" + next);
    $("#live").load("music.html #"+unit +" *"); 
    $("#preview").html("<h3 style='background:transparent; position: absolute; top:14px; right:14px;';> n e x t : </h3><img src='thumbs/"+next + ".png" + "' width='400'>")
}

////////////////////////////////////////////////

/* commented out for now as interferes with chat
need to introduce metakey...manyana... not so usefull anyway really... 

keyboard controls 
+++++++++++++++++++++++++

SPACE to toggle visibilty of preview
m to toggle visibilty of metronome
s to toggle visibilty of stopwatch
h to hide all above



function toggle_visibility(id) {
    var e = document.getElementById(id);
    if(e.style.display == 'block')
        e.style.display = 'none';
       else
           e.style.display = 'block';
}

$(document).keypress(function(e){
    // "space  bar" for next unit preview
    var checkWebkitandIE=(e.which==32 ? 1 : 0);
    var checkMoz=(e.which==32 ? 1 : 0);
    // "m" button for metronome
    var mcheckWebkitandIE=(e.which==109 ? 1 : 0);
    var mcheckMoz=(e.which==109 ? 1 : 0);
    // "s" button for stopwatch/chronometer
    var ccheckWebkitandIE=(e.which==115 ? 1 : 0);
    var ccheckMoz=(e.which==115 ? 1 : 0);

  // "h" button for hideall
    var hcheckWebkitandIE=(e.which==104 ? 1 : 0);
    var hcheckMoz=(e.which==104 ? 1 : 0);

//    console.log(e.which);
    if (checkWebkitandIE || checkMoz) { toggle_visibility('preview') }   
    if (mcheckWebkitandIE || mcheckMoz) { toggle_visibility('metro') }   
    if (ccheckWebkitandIE || ccheckMoz) { toggle_visibility('client_chronometer') }   
    if (hcheckWebkitandIE || hcheckMoz) { 
	toggle_visibility('client_chronometer') 
	toggle_visibility('metro') 
	toggle_visibility('preview') 
}
    });

*/

////////////////////////////////////////////////
// this needs to have a variable to define the websocket
// otherwise we will pings from all sockets connected
// no! the server broadcasts the ping and the clients emit the pong!
// ah but then the time reported back from the server needs to be targeted
// to specific client..
//////////////////////////////////////////////
// Latency "Pong"

socket.on("timeFromServer", function(n) { 
    socket.emit("clientTimeResponse",n);
});
socket.on("latencyFromServer", function(latency) {
    $("#client_latency").text("Latency: "+latency+"ms.")
    //console.log(latency)
});

function getLatencies(x) { socket.emit("getLatencies", x); }

//////////////////////////////////////////////


socket.on('connect', function () {
    $('#chat').addClass('connected');
});

socket.on('announcement', function (msg) {
    $('#lines').append($('<p>').append($('<em>').text(msg)));
});

socket.on('nicknames', function (nicknames) {
    $('#nicknames').empty().append($('<span>Online: </span>'));
    for (var i in nicknames) {
	$('#nicknames').append($('<b>').text(nicknames[i]));
    }
});

socket.on('user message', message);
socket.on('reconnect', function () {
    $('#lines').remove();
    message('System', 'Reconnected to the server');
});

socket.on('reconnecting', function () {
    message('System', 'Attempting to re-connect to the server');
});

socket.on('error', function (e) {
    message('System', e ? e : 'A unknown error occurred');
});

function message (from, msg) {
    $('#lines').append($('<p>').append($('<b>').text(from), msg));
}

// dom manipulation
$(function () {
    $('#set-nickname').submit(function (ev) {
	socket.emit('nickname', $('#nick').val(), function (set) {
	    if (!set) {
		clear();
		return $('#chat').addClass('nickname-set');
	    }
	    $('#nickname-err').css('visibility', 'visible');
	});
	return false;
    });

    $('#send-message').submit(function () {
	message('me', $('#message').val());
	socket.emit('user message', $('#message').val());
	clear();
	$('#lines').get(0).scrollTop = 10000000;
	return false;
    });

    function clear () {
	$('#message').val('').focus();
    };
});