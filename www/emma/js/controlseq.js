//////////////////////////////////////////////
var socket = io.connect();
//////////////////////////////////////////////


// Sequencer Controls
function startSeq() { socket.emit("startSeq") }
function stopSeq() { socket.emit("stopSeq") }
function resetSeq() { socket.emit("resetSeq") }

//////////////////////////////////////////////
// Chron Controls
function stopWatch(value) { socket.emit("stopWatch", value) }
//////////////////////////////////////////////
// Metronome Controls
//socket.on("metroPulse", metronomeTick);
function metroStart(pulse) {  socket.emit("metroStart", pulse);}
function metroStop() {  socket.emit("metroStop");}
//////////////////////////////////////////////
// Latency "Pong"
socket.on("timeFromServer", function(n) { 
    socket.emit("clientTimeResponse",n);
    //console.log(n);
});
socket.on("latencyFromServer", function(latency) {
    $("#client_latency").text(latency+"ms.")
});
function getLatencies(x) { socket.emit("getLatencies", x); }
//////////////////////////////////////////////
// Chronometer Controls
function startChr() { socket.emit("startChr"); }
function stopChr() { socket.emit("stopChr"); }
function resetChr() { socket.emit("resetChr"); 
		      $("div#client_chronometer").text("00:00:00.0"); }
socket.on("chronFromServer", function(chron){ 
    $("div#c_chronometer").text(chron); 
});

//////////////////////////////////////////////
// SEQUENCER MONITOR
socket.on("pageFlipfromserver", sequenceMonitor);
function sequenceMonitor(group, unit,time,mm,seq,unitlast){
    //    var n=6; var x=seq-1; var off=((x%n)+n)%n // thanks claudiusmaximus
    if (group == 1) { turnmeoff = "#sqr"+unitlast+".sqa"}
    if (group == 2) { turnmeoff = "#sqr"+unitlast+".sqb"}
    if (group == 3) { turnmeoff = "#sqr"+unitlast+".sqc"}
    if (group == 4) { turnmeoff = "#sqr"+unitlast+".sqd"}
    $(turnmeoff).css({'color':'black'}) 

}

// countdown to change
socket.on("countinFromServer", countinCtrl);
function countinCtrl(groupID, currentseconds,mm,text,colour,background,unit){
    //console.log("#count"+groupID)
	// all counts to control page
	$("#counttitle"+groupID).css('color','white');
	$("#counttitle"+groupID).text(text);
	$("#count"+groupID).text(currentseconds).css('color','white');
	document.getElementById("count"+groupID).style.color=colour;
}

socket.on("counterText", function(group,unit,counter,seq,unitlast,transect){  

    $("div#transect").text("Transect: " + (transect+1)); 



    if (group == 1) { $('#inner-'+unit+"-0").text(counter); 
		      $("div#unitseq0").text((transect+1)+ " : " + (seq+1)); 
		      if (counter == 0 ) { $('#inner-'+unit+"-0").css("visibility","hidden")  
					   console.log("------------------------- "+ unit)
					 }
		      else { $('#inner-'+unit+"-0").css({"color" : "white", "background":"black", "opacity" : "0.7", "border-radius":"20px", "visibility": "visible" })  }
		    }
    
    if (group == 2) { $('#inner-'+unit+"-1").text(counter);
		      $("div#unitseq1").text((transect+1)+ " : " + (seq+1)); 
		      if (counter == 0 ) { $('#inner-'+unit+"-1").css("visibility","hidden")  }
		      else { $('#inner-'+unit+"-1").css({"color":"white","background":"green", "opacity" : "0.7", "border-radius":"20px", "visibility": "visible"})  }
		    } 
 
    if (group == 3) { $('#inner-'+unit+"-2").text(counter);
		      $("div#unitseq2").text((transect+1)+ " : " + (seq+1));
		      if (counter == 0 ) { $('#inner-'+unit+"-2").css("visibility","hidden") } 
		      else { $('#inner-'+unit+"-2").css({"color":"white","background":"blue", "opacity" : "0.7", "border-radius":"20px", "visibility": "visible"})  }
		    }
    
    if (group == 4) { $('#inner-'+unit+"-3").text(counter); 
		      $("div#unitseq3").text((transect+1)+ " : " + (seq+1)); 
		      if (counter == 0 ) { $('#inner-'+unit+"-3").css("visibility","hidden")} 
		      else { $('#inner-'+unit+"-3").css({"color":"white","background":"red", "opacity" : "0.7", "border-radius":"20px", "visibility": "visible"})  }
		    } 
}
	 );

/*
//////////////////////////////////////////////
// CLient Popup window code

function newPopup(url) {
    popupWindow = window.open(
	url,'popUpWindow','height=400,width=800,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,titlebar=no,directories=no,status=yes')}

*/

//function pad2(number) { return (number < 10 ? '0' : '') + number }
//socket.on("pageFlipfromserver", pageTurn);
//function pageTurn (group,unit,time,mm) {
//    var g= pad2(group); 
//    var groupPage=document.getElementById('group').value;
   // $("#previewbox-"+group).html("<img src='svg/"+(unit) + ".svg" + "' width='515'>")
//   }

