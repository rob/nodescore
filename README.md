# NODESCORE: A netscore playout server.

## NODESCORE SERVER INSTALLATION 
installation for server running debian stable or testing OS other systems see here: https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager

### get nodejs dependencies
```
apt-get update 
apt-get install git-core curl build-essential openssl libssl-dev
```
//////////////////////////////////
// grab and compile from source
```
mkdir -p /tmp/build/node && cd /tmp/build/node
git clone https://github.com/joyent/node.git .
git checkout v0.8.0
./configure --openssl-libpath=/usr/lib/ssl
make
make test
make install
node -v
```
### install server-side  dependencies
```
aptitude install phantomjs
aptitude install imagemagick
aptitude install python-qt4 libqt4-webkit python-pip
aptitude install xvfb xbase-clients xfonts-base libgtk2.0-0
// get nodescore dependencies
// the node_modules dir should live in the require.path 
sudo npm install socket.io jsdom jQuery xmlhttprequest node-static requirejs
sudo npm -g install supervisor
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10
```

### install  nodescore 
git clone git@git.kiben.net:rc/nodescore.git
### start server
```
cd nodescore
node nodescore.js
```

## generating material from old magick system and importing to nodescore:

flow:

cd ../code/magick

edit ./svg_gen.sh to specify instrument and target location

./svg_gen.sh

for f in *.svg ; do mv $f ${f#???}; done

count=0 ;for i in [0-7]*.svg ; do count=$((count+1)) ; echo mv $i  $count.svg ; done

