////////////////////////////////////////////
// Chronometer
////////////////////////////////////////////

// number padding: 0 to 00
function pad(number) { return (number < 10 ? '0' : '') + number }

function xdateTime() { 
    var xdatetime= new Date(); 
    var now=xdatetime.toString()
    return now
}


// the chronometer initial states
function zeroChron(){
    zecsec = 0;  seconds = 0; 
    mins = 0;  hours = 0;
    zero = pad(hours) +":"+pad(mins)+ ':'+ pad(seconds)
    chron = zero
   return zero
}

zeroChron()

/*
function chronCtrl (state,interval){
	console.log("=========================== chronstate=" + state)
	if (state==1){
	    var date = new Date()
	    var starttime = new Date().getTime() / 1000;
	    //var interval = 1020 - date.getMilliseconds();
	    xstopwatch =  setInterval(function () {
		var nowtime = new Date().getTime() / 1000;
		now = nowtime-starttime
		hours = parseInt( now / 3600 ) % 24;
		minutes = parseInt( now / 60 ) % 60;
		seconds = parseInt(now  % 60);
		milliseconds = Math.floor((now-seconds)*10)%60;
		time = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds) + "."+milliseconds;
		console.log(time)
//		socket.broadcast.emit('chronFromServer', time)
//		socket.emit('chronFromServer', time)
	    }, 100)
    }
	if (state==0) {
	    clearInterval(xstopwatch);
	     }	
    }
*/
//exports.chronCtrl = chronCtrl;
exports.xdateTime = xdateTime;
exports.zeroChron = zeroChron;
