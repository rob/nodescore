#!/bin/sh

#sudo dpkg-reconfigure dash
# use bash instead of dash on ubuntu systems

#sudo apt-get install phantomjs supervisor
#npm install sockets.io node-static requirejs

mkdir -p /usr/local/bin
cp -pa nodescore /usr/local/bin/

mkdir /usr/share/nodescore
cp -pa www *.js /usr/share/nodescore/
chown -R  rob:www-data /usr/share/nodescore/
cp -pa nodescored /etc/init.d/
chmod 755 /etc/init.d/nodescored
update-rc.d nodescored defaults
