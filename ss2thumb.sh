#!/bin/bash

# calls phantomjs to make screenshots 
# of each anchor in PROJECT then calls
# imagemagick to resize them for preview
# panel. The size of the screenshot 
# is defined in rasterize.js

# nodescore@kiben.net
# nodescore.kiben.net

SERVER="http://10.80.80.130:8890"
BASEDIR='8'
PROJECT=$1
THUMBPATH="www/$BASEDIR/thumbs"

if [ ! -d $THUMBPATH ];
    echo creating $THUMBPATH
    then mkdir -p $THUMBPATH;
fi

ANCHORS=`grep '<a' www/$BASEDIR/music.html|wc -l`
GAP=330
TOP=$(($GAP*-1))

for each in $(seq 1 $ANCHORS);

do
    let each=each-1
    echo $TOP
    TOP=$(($TOP+$GAP));
    echo phantomjs rasterize.js $SERVER/$BASEDIR/music.html#$each $each.png $TOP
    phantomjs rasterize.js $SERVER/$BASEDIR/music.html#$each $each.png $TOP
    #mogrify -resize 400x  $each.png
    mv $each.png www/$BASEDIR/thumbs/

done
